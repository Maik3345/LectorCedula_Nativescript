import { Component, OnInit } from "@angular/core";

import { Item } from "./item";
import { ItemService } from "./item.service";
import { BarcodeScanner } from 'nativescript-barcodescanner';
// import { ZXing } from 'nativescript-zxing';

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html",
})
export class ItemsComponent implements OnInit {
    items: Item[];

    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class. 
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    constructor(private itemService: ItemService,
        private barcodeScanner: BarcodeScanner) { }
    public blockBlue = false;
    public textCaptured: string = "Capture una cedula";
    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }

    viewAlert(): void {
        alert("Helloow!!");
        this.blockBlue = !this.blockBlue
    }
    hex2a(hex) {
        var str = '';
        for (var i = 0; i < hex.length; i += 2) {
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        }
        return str;
    }
    public onScan() {
        let option = {
            formats: "PDF_417",   // Pass in of you want to restrict scanning to certain types
            cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
            cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
            message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
            showFlipCameraButton: true,   // default false
            preferFrontCamera: false,     // default false
            showTorchButton: true,        // default false
            beepOnScan: true,             // Play or Suppress beep on scan (default true)
            torchOn: false,               // launch with the flashlight on (default false)
            resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
            orientation: "landscape",     // Android only, optionally lock the orientation to either "portrait" or "landscape"
            openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
        }
        this.barcodeScanner.scan(option).then((result) => {
            this.viewAlertScan(result);
        }, (errorMessage) => {
            console.log("Error when scanning " + errorMessage);
        });


    }


    public viewAlertScan(result) {
        // this.textCaptured = result.text(JSON.stringify(ZXing.PDF417.PDF417Reader.decode(bitmap, null, false), null, 4));;
        // var ZXing = require('nativescript-zxing');
        // var zx = new ZXing();
        // console.log(zx)
        // var options = { tryHarder: true, formats: [ZXing.PDF_417] };
        // var results = zx.decodeBarcode(result.text, options);
        // if (!results) {
        //     console.log("Unable to decode barcode");
        // } else {
        //     console.log("Barcode format", results.format);
        //     console.log("Barcode value", results.barcode);
        // }
        this.textCaptured = result.text;
        setTimeout(() => {
            alert({
                title: "You Scanned ",
                message: "Format: " + result.format + ",\nContent: " + result.text,
                okButtonText: "OK"
            });
        }, 2000)
    }


}