"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./item.service");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
// import { ZXing } from 'nativescript-zxing';
var ItemsComponent = (function () {
    // This pattern makes use of Angular’s dependency injection implementation to inject an instance of the ItemService service into this class. 
    // Angular knows about this service because it is included in your app’s main NgModule, defined in app.module.ts.
    function ItemsComponent(itemService, barcodeScanner) {
        this.itemService = itemService;
        this.barcodeScanner = barcodeScanner;
        this.blockBlue = false;
        this.textCaptured = "Capture una cedula";
    }
    ItemsComponent.prototype.ngOnInit = function () {
        this.items = this.itemService.getItems();
    };
    ItemsComponent.prototype.viewAlert = function () {
        alert("Helloow!!");
        this.blockBlue = !this.blockBlue;
    };
    ItemsComponent.prototype.hex2a = function (hex) {
        var str = '';
        for (var i = 0; i < hex.length; i += 2) {
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        }
        return str;
    };
    ItemsComponent.prototype.onScan = function () {
        var _this = this;
        var option = {
            formats: "PDF_417",
            cancelLabel: "EXIT. Also, try the volume buttons!",
            cancelLabelBackgroundColor: "#333333",
            message: "Use the volume buttons for extra light",
            showFlipCameraButton: true,
            preferFrontCamera: false,
            showTorchButton: true,
            beepOnScan: true,
            torchOn: false,
            resultDisplayDuration: 500,
            orientation: "landscape",
            openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
        };
        this.barcodeScanner.scan(option).then(function (result) {
            _this.viewAlertScan(result);
        }, function (errorMessage) {
            console.log("Error when scanning " + errorMessage);
        });
    };
    ItemsComponent.prototype.viewAlertScan = function (result) {
        // this.textCaptured = result.text(JSON.stringify(ZXing.PDF417.PDF417Reader.decode(bitmap, null, false), null, 4));;
        // var ZXing = require('nativescript-zxing');
        // var zx = new ZXing();
        // console.log(zx)
        // var options = { tryHarder: true, formats: [ZXing.PDF_417] };
        // var results = zx.decodeBarcode(result.text, options);
        // if (!results) {
        //     console.log("Unable to decode barcode");
        // } else {
        //     console.log("Barcode format", results.format);
        //     console.log("Barcode value", results.barcode);
        // }
        this.textCaptured = result.text;
        setTimeout(function () {
            alert({
                title: "You Scanned ",
                message: "Format: " + result.format + ",\nContent: " + result.text,
                okButtonText: "OK"
            });
        }, 2000);
    };
    return ItemsComponent;
}());
ItemsComponent = __decorate([
    core_1.Component({
        selector: "ns-items",
        moduleId: module.id,
        templateUrl: "./items.component.html",
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService,
        nativescript_barcodescanner_1.BarcodeScanner])
], ItemsComponent);
exports.ItemsComponent = ItemsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBR2xELCtDQUE2QztBQUM3QywyRUFBNkQ7QUFDN0QsOENBQThDO0FBTzlDLElBQWEsY0FBYztJQUd2Qiw2SUFBNkk7SUFDN0ksaUhBQWlIO0lBQ2pILHdCQUFvQixXQUF3QixFQUNoQyxjQUE4QjtRQUR0QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUNoQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDbkMsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixpQkFBWSxHQUFXLG9CQUFvQixDQUFDO0lBRkwsQ0FBQztJQUcvQyxpQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzdDLENBQUM7SUFFRCxrQ0FBUyxHQUFUO1FBQ0ksS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ25CLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFBO0lBQ3BDLENBQUM7SUFDRCw4QkFBSyxHQUFMLFVBQU0sR0FBRztRQUNMLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNiLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7WUFDckMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDL0QsQ0FBQztRQUNELE1BQU0sQ0FBQyxHQUFHLENBQUM7SUFDZixDQUFDO0lBQ00sK0JBQU0sR0FBYjtRQUFBLGlCQXNCQztRQXJCRyxJQUFJLE1BQU0sR0FBRztZQUNULE9BQU8sRUFBRSxTQUFTO1lBQ2xCLFdBQVcsRUFBRSxxQ0FBcUM7WUFDbEQsMEJBQTBCLEVBQUUsU0FBUztZQUNyQyxPQUFPLEVBQUUsd0NBQXdDO1lBQ2pELG9CQUFvQixFQUFFLElBQUk7WUFDMUIsaUJBQWlCLEVBQUUsS0FBSztZQUN4QixlQUFlLEVBQUUsSUFBSTtZQUNyQixVQUFVLEVBQUUsSUFBSTtZQUNoQixPQUFPLEVBQUUsS0FBSztZQUNkLHFCQUFxQixFQUFFLEdBQUc7WUFDMUIsV0FBVyxFQUFFLFdBQVc7WUFDeEIsMkNBQTJDLEVBQUUsSUFBSSxDQUFDLG1GQUFtRjtTQUN4SSxDQUFBO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBTTtZQUN6QyxLQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBRSxVQUFDLFlBQVk7WUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixHQUFHLFlBQVksQ0FBQyxDQUFDO1FBQ3ZELENBQUMsQ0FBQyxDQUFDO0lBR1AsQ0FBQztJQUdNLHNDQUFhLEdBQXBCLFVBQXFCLE1BQU07UUFDdkIsb0hBQW9IO1FBQ3BILDZDQUE2QztRQUM3Qyx3QkFBd0I7UUFDeEIsa0JBQWtCO1FBQ2xCLCtEQUErRDtRQUMvRCx3REFBd0Q7UUFDeEQsa0JBQWtCO1FBQ2xCLCtDQUErQztRQUMvQyxXQUFXO1FBQ1gscURBQXFEO1FBQ3JELHFEQUFxRDtRQUNyRCxJQUFJO1FBQ0osSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hDLFVBQVUsQ0FBQztZQUNQLEtBQUssQ0FBQztnQkFDRixLQUFLLEVBQUUsY0FBYztnQkFDckIsT0FBTyxFQUFFLFVBQVUsR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLGNBQWMsR0FBRyxNQUFNLENBQUMsSUFBSTtnQkFDbEUsWUFBWSxFQUFFLElBQUk7YUFDckIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFBO0lBQ1osQ0FBQztJQUdMLHFCQUFDO0FBQUQsQ0FBQyxBQXpFRCxJQXlFQztBQXpFWSxjQUFjO0lBTDFCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLHdCQUF3QjtLQUN4QyxDQUFDO3FDQU1tQywwQkFBVztRQUNoQiw0Q0FBYztHQU5qQyxjQUFjLENBeUUxQjtBQXpFWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuaW1wb3J0IHsgSXRlbSB9IGZyb20gXCIuL2l0ZW1cIjtcbmltcG9ydCB7IEl0ZW1TZXJ2aWNlIH0gZnJvbSBcIi4vaXRlbS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBCYXJjb2RlU2Nhbm5lciB9IGZyb20gJ25hdGl2ZXNjcmlwdC1iYXJjb2Rlc2Nhbm5lcic7XG4vLyBpbXBvcnQgeyBaWGluZyB9IGZyb20gJ25hdGl2ZXNjcmlwdC16eGluZyc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcIm5zLWl0ZW1zXCIsXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICB0ZW1wbGF0ZVVybDogXCIuL2l0ZW1zLmNvbXBvbmVudC5odG1sXCIsXG59KVxuZXhwb3J0IGNsYXNzIEl0ZW1zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBpdGVtczogSXRlbVtdO1xuXG4gICAgLy8gVGhpcyBwYXR0ZXJuIG1ha2VzIHVzZSBvZiBBbmd1bGFy4oCZcyBkZXBlbmRlbmN5IGluamVjdGlvbiBpbXBsZW1lbnRhdGlvbiB0byBpbmplY3QgYW4gaW5zdGFuY2Ugb2YgdGhlIEl0ZW1TZXJ2aWNlIHNlcnZpY2UgaW50byB0aGlzIGNsYXNzLiBcbiAgICAvLyBBbmd1bGFyIGtub3dzIGFib3V0IHRoaXMgc2VydmljZSBiZWNhdXNlIGl0IGlzIGluY2x1ZGVkIGluIHlvdXIgYXBw4oCZcyBtYWluIE5nTW9kdWxlLCBkZWZpbmVkIGluIGFwcC5tb2R1bGUudHMuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpdGVtU2VydmljZTogSXRlbVNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgYmFyY29kZVNjYW5uZXI6IEJhcmNvZGVTY2FubmVyKSB7IH1cbiAgICBwdWJsaWMgYmxvY2tCbHVlID0gZmFsc2U7XG4gICAgcHVibGljIHRleHRDYXB0dXJlZDogc3RyaW5nID0gXCJDYXB0dXJlIHVuYSBjZWR1bGFcIjtcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5pdGVtcyA9IHRoaXMuaXRlbVNlcnZpY2UuZ2V0SXRlbXMoKTtcbiAgICB9XG5cbiAgICB2aWV3QWxlcnQoKTogdm9pZCB7XG4gICAgICAgIGFsZXJ0KFwiSGVsbG9vdyEhXCIpO1xuICAgICAgICB0aGlzLmJsb2NrQmx1ZSA9ICF0aGlzLmJsb2NrQmx1ZVxuICAgIH1cbiAgICBoZXgyYShoZXgpIHtcbiAgICAgICAgdmFyIHN0ciA9ICcnO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGhleC5sZW5ndGg7IGkgKz0gMikge1xuICAgICAgICAgICAgc3RyICs9IFN0cmluZy5mcm9tQ2hhckNvZGUocGFyc2VJbnQoaGV4LnN1YnN0cihpLCAyKSwgMTYpKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc3RyO1xuICAgIH1cbiAgICBwdWJsaWMgb25TY2FuKCkge1xuICAgICAgICBsZXQgb3B0aW9uID0ge1xuICAgICAgICAgICAgZm9ybWF0czogXCJQREZfNDE3XCIsICAgLy8gUGFzcyBpbiBvZiB5b3Ugd2FudCB0byByZXN0cmljdCBzY2FubmluZyB0byBjZXJ0YWluIHR5cGVzXG4gICAgICAgICAgICBjYW5jZWxMYWJlbDogXCJFWElULiBBbHNvLCB0cnkgdGhlIHZvbHVtZSBidXR0b25zIVwiLCAvLyBpT1Mgb25seSwgZGVmYXVsdCAnQ2xvc2UnXG4gICAgICAgICAgICBjYW5jZWxMYWJlbEJhY2tncm91bmRDb2xvcjogXCIjMzMzMzMzXCIsIC8vIGlPUyBvbmx5LCBkZWZhdWx0ICcjMDAwMDAwJyAoYmxhY2spXG4gICAgICAgICAgICBtZXNzYWdlOiBcIlVzZSB0aGUgdm9sdW1lIGJ1dHRvbnMgZm9yIGV4dHJhIGxpZ2h0XCIsIC8vIEFuZHJvaWQgb25seSwgZGVmYXVsdCBpcyAnUGxhY2UgYSBiYXJjb2RlIGluc2lkZSB0aGUgdmlld2ZpbmRlciByZWN0YW5nbGUgdG8gc2NhbiBpdC4nXG4gICAgICAgICAgICBzaG93RmxpcENhbWVyYUJ1dHRvbjogdHJ1ZSwgICAvLyBkZWZhdWx0IGZhbHNlXG4gICAgICAgICAgICBwcmVmZXJGcm9udENhbWVyYTogZmFsc2UsICAgICAvLyBkZWZhdWx0IGZhbHNlXG4gICAgICAgICAgICBzaG93VG9yY2hCdXR0b246IHRydWUsICAgICAgICAvLyBkZWZhdWx0IGZhbHNlXG4gICAgICAgICAgICBiZWVwT25TY2FuOiB0cnVlLCAgICAgICAgICAgICAvLyBQbGF5IG9yIFN1cHByZXNzIGJlZXAgb24gc2NhbiAoZGVmYXVsdCB0cnVlKVxuICAgICAgICAgICAgdG9yY2hPbjogZmFsc2UsICAgICAgICAgICAgICAgLy8gbGF1bmNoIHdpdGggdGhlIGZsYXNobGlnaHQgb24gKGRlZmF1bHQgZmFsc2UpXG4gICAgICAgICAgICByZXN1bHREaXNwbGF5RHVyYXRpb246IDUwMCwgICAvLyBBbmRyb2lkIG9ubHksIGRlZmF1bHQgMTUwMCAobXMpLCBzZXQgdG8gMCB0byBkaXNhYmxlIGVjaG9pbmcgdGhlIHNjYW5uZWQgdGV4dFxuICAgICAgICAgICAgb3JpZW50YXRpb246IFwibGFuZHNjYXBlXCIsICAgICAvLyBBbmRyb2lkIG9ubHksIG9wdGlvbmFsbHkgbG9jayB0aGUgb3JpZW50YXRpb24gdG8gZWl0aGVyIFwicG9ydHJhaXRcIiBvciBcImxhbmRzY2FwZVwiXG4gICAgICAgICAgICBvcGVuU2V0dGluZ3NJZlBlcm1pc3Npb25XYXNQcmV2aW91c2x5RGVuaWVkOiB0cnVlIC8vIE9uIGlPUyB5b3UgY2FuIHNlbmQgdGhlIHVzZXIgdG8gdGhlIHNldHRpbmdzIGFwcCBpZiBhY2Nlc3Mgd2FzIHByZXZpb3VzbHkgZGVuaWVkXG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5iYXJjb2RlU2Nhbm5lci5zY2FuKG9wdGlvbikudGhlbigocmVzdWx0KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnZpZXdBbGVydFNjYW4ocmVzdWx0KTtcbiAgICAgICAgfSwgKGVycm9yTWVzc2FnZSkgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciB3aGVuIHNjYW5uaW5nIFwiICsgZXJyb3JNZXNzYWdlKTtcbiAgICAgICAgfSk7XG5cblxuICAgIH1cblxuXG4gICAgcHVibGljIHZpZXdBbGVydFNjYW4ocmVzdWx0KSB7XG4gICAgICAgIC8vIHRoaXMudGV4dENhcHR1cmVkID0gcmVzdWx0LnRleHQoSlNPTi5zdHJpbmdpZnkoWlhpbmcuUERGNDE3LlBERjQxN1JlYWRlci5kZWNvZGUoYml0bWFwLCBudWxsLCBmYWxzZSksIG51bGwsIDQpKTs7XG4gICAgICAgIC8vIHZhciBaWGluZyA9IHJlcXVpcmUoJ25hdGl2ZXNjcmlwdC16eGluZycpO1xuICAgICAgICAvLyB2YXIgenggPSBuZXcgWlhpbmcoKTtcbiAgICAgICAgLy8gY29uc29sZS5sb2coengpXG4gICAgICAgIC8vIHZhciBvcHRpb25zID0geyB0cnlIYXJkZXI6IHRydWUsIGZvcm1hdHM6IFtaWGluZy5QREZfNDE3XSB9O1xuICAgICAgICAvLyB2YXIgcmVzdWx0cyA9IHp4LmRlY29kZUJhcmNvZGUocmVzdWx0LnRleHQsIG9wdGlvbnMpO1xuICAgICAgICAvLyBpZiAoIXJlc3VsdHMpIHtcbiAgICAgICAgLy8gICAgIGNvbnNvbGUubG9nKFwiVW5hYmxlIHRvIGRlY29kZSBiYXJjb2RlXCIpO1xuICAgICAgICAvLyB9IGVsc2Uge1xuICAgICAgICAvLyAgICAgY29uc29sZS5sb2coXCJCYXJjb2RlIGZvcm1hdFwiLCByZXN1bHRzLmZvcm1hdCk7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZyhcIkJhcmNvZGUgdmFsdWVcIiwgcmVzdWx0cy5iYXJjb2RlKTtcbiAgICAgICAgLy8gfVxuICAgICAgICB0aGlzLnRleHRDYXB0dXJlZCA9IHJlc3VsdC50ZXh0O1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIGFsZXJ0KHtcbiAgICAgICAgICAgICAgICB0aXRsZTogXCJZb3UgU2Nhbm5lZCBcIixcbiAgICAgICAgICAgICAgICBtZXNzYWdlOiBcIkZvcm1hdDogXCIgKyByZXN1bHQuZm9ybWF0ICsgXCIsXFxuQ29udGVudDogXCIgKyByZXN1bHQudGV4dCxcbiAgICAgICAgICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIlxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sIDIwMDApXG4gICAgfVxuXG5cbn0iXX0=